# Changelog

## [1.4.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/compare/release/1.3.0...release/1.4.0) (2025-03-05)


### Features

* publish new helm for v1.125.0-eole3.0 ([12553b9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/12553b9635e726d5ba19f413de4a493d761536ac))

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/compare/release/1.2.1...release/1.3.0) (2025-02-04)


### Features

* add auto join rooms feature ([55e1df0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/55e1df02cac6638805466374ede07cea66ebac27))

### [1.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/compare/release/1.2.0...release/1.2.1) (2024-10-17)


### Bug Fixes

* deploy synapse 1.117.0-eole3.0 ([a78ad02](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/a78ad02ffc0a448de9338cb7fe33987fe4714324))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-05-03)


### Features

* deploy eole appversion 1.106.0 ([5b2d49c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/5b2d49c34b55e8be6557b371793b63773078dbd1))
* deploy eole appversion 1.106.0 ([db26ba3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/db26ba3ecf2f7099370fe0f07be96d1fd60425bc))


### Bug Fixes

* deploy new version 1.106.0 ([8fe8c60](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/8fe8c60647bec471b17e110b7039f0d55c31f8d0))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/compare/release/1.0.0...release/1.1.0) (2024-04-16)


### Features

* encryption e2ee is off by default ([a1fae6a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/a1fae6a45dc42178f646eae32be22887778817e3))


### Bug Fixes

* rebuild helm ([3de5f2e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/3de5f2e09b021122c56c9bec0ee54575f6d6d792))

## 1.0.0 (2024-04-05)


### Features

* add s3 support ([ce8235c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/ce8235c52f62aa27e0c945688dd80015114218f4))
* first stable version ([bf883ad](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/bf883ad653ba54e24affe43056055995f0db640a))
* first testing version ([254ce35](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/254ce3511a5a1d0f556e7848e6d264a2ef9b764d))
* reorganise parameters and add rooms federation ([b6de868](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/b6de86873d2f87df270d3f86361bf5cb5dcca1e1))
* set default timezone to europe/paris ([bf89521](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/bf895210842f1daee8eedfb71b24f3ae68d053c3))
* **synapse:** first helm version for synapse server ([1f343d9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/1f343d9ea392fc0459e096892b063cb28366c310))


### Bug Fixes

* add federation ([cece95b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/cece95b996579315dee244fd58c3f1cfa143676f))
* add s3-secret.yaml ([380d872](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/380d87232e9c73ce76069c4baf98369cc3618d77))
* split registry and repository parameters ([5cdc01a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/synapse/synapse-helm-chart/commit/5cdc01afe34200a98f68d52e188455a2beea8ffb))
